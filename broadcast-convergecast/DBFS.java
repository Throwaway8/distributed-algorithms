import java.io.*;
import java.util.*;

// non good copy

public class DBFS extends Algorithm {

    public Object run() {
        // Invoke the main algorithm, passing as parameter the node's id.
        String lastmsg = bfsTrees(getID());
        return lastmsg;
    }

    public String bfsTrees(String id) {
        Vector<String> v = neighbours();



        // Message mssg = null;
        Message m = null;
        String parenta = "";
        String parentb = "";
        String roota = "1";
        String rootb = "2";

        Boolean end_flaga = false;
        int end_cnta = 0;
        Boolean end_flagb = false;
        int end_cntb = 0;

        String dataa = "";
        String datab = "";
        
        // String ack = ""
        int n = numNeighbours();
        String[] childrena = new String[n];
        String[] childrenb = new String[n];
        Arrays.fill(childrena, "");
        Arrays.fill(childrenb, "");
        
        Message[] mssga = new Message[n];
        Message[] mssgb = new Message[n];
        Arrays.fill(mssga, null);
        Arrays.fill(mssgb, null);

        boolean acka = false;
        boolean[] reqa = new boolean[n];
        Arrays.fill(reqa, false);

        boolean ackb = false;
        boolean[] reqb = new boolean[n];
        Arrays.fill(reqb, false);

        

        if (equal(id, "1")) {
 
            dataa = id + "," + "req" + "," + id;
            
 
            for (int i = 0; i < v.size(); i++) {
        
                parenta = "self";
                Message mssg_ = makeMessage(v.elementAt(i), dataa);
                mssga[i] = mssg_;
               
            }
        }

        if (equal(id, "2")) {
      
            datab = id + "," + "req" + "," + id;
            for (int i = 0; i < v.size(); i++) {
                parentb = "self";
                Message mssg_ = makeMessage(v.elementAt(i), datab);
                mssgb[i] = mssg_;
            }
        }

        
        try {
            
            
            // main loop starts
            while (waitForNextRound()) {  
                
                // ===Send Phase===
                for (int i = 0; i < n; i++) {

                    if (mssga[i] != null) {
                        send(mssga[i]);
                    }
                    if (mssgb[i] != null) {
                        send(mssgb[i]);
                    }
                }

                for (int i = 0; i < n; i++) {
                    mssga[i] = null;
                    mssgb[i] = null;
                }

                if ((end_flaga == true || end_cnta >= 2) && (end_flagb == true || end_cntb >= 2)) {
                    
                    return "root 1: " + parenta + " " + Arrays.toString(childrena) + " root 2: " + parentb + " " + Arrays.toString(childrenb);
                }

                // ===Receive Phase===
                boolean cont = true;
                int cnt = 0;
                while (cont == true) {
                    m = receive();

                    if (m != null) {
                        if (equal(id, "11")) {
                            printMessage("geeze " + m.data().split(",")[1]);
                            
                        }
                        
                        // search tree a
                        if (equal(m.data().split(",")[2], roota)) {

                            if (equal(m.data().split(",")[1], "req") && parenta.equals("")) {
                                acka = true;
                                
                                parenta = m.data().split(",")[0];
                       
                                for (int i = 0; i < n; i++) {
                       
                                    if (i != v.indexOf(parenta)) {
                                        
                                        dataa = id + "," + "req" + "," + m.data().split(",")[2];
                                        mssga[i] = makeMessage(v.elementAt(i), dataa);
                                    }
                                    else {
                                        printMessage("FUCK");
                                        dataa = id + "," + "ack" + "," + m.data().split(",")[2];
                                        mssga[i] = makeMessage(v.elementAt(i), dataa);   
                                    }
                                    
                                }
                                
                            }

                            if (equal(m.data().split(",")[1], "ack")) {
                               
                                childrena[v.indexOf(m.data().split(",")[0])] = m.data().split(",")[0];
                                end_flaga = true;
                                
                            }

                            if (acka == true) {
                                end_cnta++;
                            }


                        }

                        // search tree b
                        if (equal(m.data().split(",")[2], rootb)) {

                            if (equal(m.data().split(",")[1], "req") && parentb.equals("")) {
                                ackb = true;
                                parentb = m.data().split(",")[0];
                       
                                for (int i = 0; i < n; i++) {
                       
                                    if (i != v.indexOf(parentb)) {
                                        
                                        datab = id + "," + "req" + "," + m.data().split(",")[2];
                                        mssgb[i] = makeMessage(v.elementAt(i), datab);
                                    }
                                    else {
                                        // printMessage("FUCK");
                                        datab = id + "," + "ack" + "," + m.data().split(",")[2];
                                        mssgb[i] = makeMessage(v.elementAt(i), datab);   
                                    }
                                }
                            }

                            if (equal(m.data().split(",")[1], "ack")) {
                                
                                childrenb[v.indexOf(m.data().split(",")[0])] = m.data().split(",")[0];
                                end_flagb = true;

                                if (equal(id, "11")) {
                                    printMessage("bahhhh " + m.data().split(",")[1] + end_flaga + " " + end_flagb + " " + end_cnta + " " + end_cntb);
                                }
                                
                            }

                            if (ackb == true) {
                                end_cntb++;
                            }
                        }
                    }
                    else {
                        cont = false;
                    }
                    cnt++;
                }

            }
        } catch(SimulatorException e){
            System.out.println("ERROR: "+e.getMessage());
        }
        
        return null;
    }
    
    /* Print information about the parent and children of this processor in both BFS trees */
    private void printParentsChildren(String parent1, String parent2, Vector<String>children1,
                                      Vector<String>children2) {
	String outMssg = "["+parent1+":";
	for (int i = 0; i < children1.size()-1; ++i) 
		outMssg = outMssg + children1.elementAt(i)+" ";
	if (children1.size() > 0)
		outMssg = outMssg + children1.elementAt(children1.size()-1) + "] [" + parent2 + ":";
	else outMssg = outMssg + "] ["+parent2+":";
	for (int i = 0; i < children2.size()-1; ++i) 
		outMssg = outMssg + children2.elementAt(i)+" ";	
	if (children2.size() > 0)					
		outMssg = outMssg + children2.elementAt(children2.size()-1)+ "]";	
	else outMssg = outMssg + "]";
	showMessage(outMssg);
	printMessage(outMssg);        
    }
}