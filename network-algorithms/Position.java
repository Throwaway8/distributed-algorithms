import java.util.Vector; //We need this for the Vector class.
import java.io.FileWriter; 
import java.io.*;
import java.util.*;

public class Position extends Algorithm {
    public Object run() {
        String status = findPosition(getID());
        return status;
    }


    public String findPosition(String id) {
        // ===Initial Setup===
        Vector<String> v = neighbours();
        String leftNeighbour = (String) v.elementAt(0); 
        String rightNeighbour = (String) v.elementAt(1); 
        int cnt = 1;
        String orig = id;
        String data = id + "," + cnt;
        boolean left_done = false;
        boolean right_done = false;
        String ret_val = "0";
        // Vector<Message> send_messages = new Vector<Message>();
        // printMessage(leftNeighbour);
        Message mssg = null;
        if (leftNeighbour != "0"){
            mssg = makeMessage(leftNeighbour, data);
        }
        Message mssg2 = null;
        // send_messages.add(mssg);
        // Vector<Message> receive_messages = new Vector<Message>();
        Message m = null;
        // receive_messages.add(mssg);
        String status = "unkown";
        
        // ===Main Loop===
        try {

            // printMessage(mssg.source());
            while (waitForNextRound()) {
                // ===Send Phase===
                if(mssg != null) { // If we have a message, send it!
                    // send(mssg); 
                    send(mssg); 
                    if (contains(mssg.data(), "DONE")) {
                        return ret_val;
                    }
                }
                int tmp = 0;
                mssg = null;
                m = receive();
                if(m != null) {
                    // printMessage(m.source());
                    // send left
                    if ((equal(m.source(), rightNeighbour) && m.data().split(",")[0] != "DONE" && leftNeighbour != "0")) {
 
                        tmp = Integer.parseInt(m.data().split(",")[1]);
                        tmp = tmp + 1;

                        data = m.data().split(",")[0] + "," + tmp;
                        tmp = 0;
                        mssg = makeMessage(leftNeighbour, data);
                    }
                    // sending right if end node with no left neighbour
                    if (equal(m.source(), rightNeighbour) && leftNeighbour == "0" && m.data().split(",")[0] != "DONE") {
                        data = m.data().split(",")[0] + "," + m.data().split(",")[1];
                        mssg = makeMessage(rightNeighbour, data);
                    }

                    // messages being set west if they receive a message with their id stop sending messages
                    if (equal(m.source(), leftNeighbour) && leftNeighbour != "0" && m.data().split(",")[0] != id && rightNeighbour != "0") {

                        data = m.data().split(",")[0] + "," + m.data().split(",")[1];
                        mssg = makeMessage(rightNeighbour, data);
                        if (equal(m.data().split(",")[0], getID())) {
                            ret_val = m.data().split(",")[1];
                            mssg = null;
                        }
                    }
                    // the first node in the chain, if it recevies it sends the first done msg 
                    if (equal(m.source(), leftNeighbour) && rightNeighbour == "0") {

                        ret_val = m.data().split(",")[1];
                        mssg = makeMessage(leftNeighbour, "DONE," +  m.data().split(",")[1]);
                    } 
                    // if the message contains done return left neighbours and finish
                    if (equal(m.data().split(",")[0], "DONE")) {
                        if (leftNeighbour == "0") {
                            return ret_val;
                        }
                        // mssg = makeMessage(leftNeighbour, "DONE");
                    }

                }

            }
        } catch(SimulatorException e){
                System.out.println("ERROR: " + e.toString());

        }
        return null;
    }
}